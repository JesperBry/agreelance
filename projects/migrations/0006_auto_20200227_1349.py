# Generated by Django 2.1.7 on 2020-02-27 13:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0005_auto_20200227_1335'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='rating',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='projects.Rating'),
            preserve_default=False,
        ),
    ]
