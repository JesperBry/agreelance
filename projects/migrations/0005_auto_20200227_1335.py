# Generated by Django 2.1.7 on 2020-02-27 13:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0004_auto_20200227_1332'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='rating',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='projects.Rating'),
        ),
    ]
