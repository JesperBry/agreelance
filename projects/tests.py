from django.test import TestCase, Client, RequestFactory, override_settings
from django.urls import reverse
from user.models import Profile
from django.contrib.auth.models import User
from .models import Project, Task, TaskFile, TaskOffer, Delivery, ProjectCategory, Team, TaskFileTeam, directory_path, Rating
from .forms import ProjectForm, TaskFileForm, ProjectStatusForm, TaskOfferForm, TaskOfferResponseForm, TaskPermissionForm, DeliveryForm, TaskDeliveryResponseForm, TeamForm, TeamAddForm
from .views import project_view, get_user_task_permissions, create_review_dict
from projects.templatetags.project_extras import get_all_taskoffers, get_accepted_task_offer, check_taskoffers, get_project_participants_string, get_project_participants
from django.contrib import auth
from unittest import skip

# Create your tests here.


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()

        # Project owner
        self.user = User.objects.create_user(
            username='testUser',
            email='test@test.com',
            password='top_secret'
        )

        # Bidder user
        self.user_2 = User.objects.create_user(
            username='testUser_2',
            email='test@test.com',
            password='top_secret'
        )

        self.testCategory = ProjectCategory.objects.create(name='test')

        self.testProject = Project.objects.create(
            user=User.objects.get(username='testUser').profile,
            title="Test prosjekt",
            description='',
            category=self.testCategory,
            status='open'
        )

        self.testProjectTwo = Project.objects.create(
            user=User.objects.get(username='testUser').profile,
            title="Another test project",
            description='',
            category=self.testCategory,
            status='i'
        )

        self.testTaskTwo = Task.objects.create(
            project=self.testProjectTwo,
            title='Test task 2',
            description='',
            budget=200,
            status='pp',
            feedback=''
        )

        self.testTask = Task.objects.create(
            project=self.testProject,
            title='Test task',
            description='',
            budget=20,
            status='ad',
            feedback=''
        )

        # Valid data
        self.data = {
            'title': 'Offer test',
            'description': 'This is a description',
            'price': 100,
            'taskvalue': Task.objects.last().pk,
            'offer_submit': True
        }

    def test_total_budget(self):
        tasks = self.testProject.tasks.all()
        total_budget = 0

        for item in tasks:
            total_budget += item.budget
        self.assertEqual(total_budget, 20)

    def test_GET_view_projects(self):
        response = self.client.get('/projects/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'projects/projects.html')

    def test_project_view_GET_project_1(self):
        response = self.client.get('/projects/1/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'projects/project_view.html')

    def test_project_view_POST_offer_submit(self):
        self.client.login(username='testUser_2', password='top_secret')
        response = self.client.post('/projects/1/', {
            'title': 'test offer',
            'description': 'this is a description',
            'price': 29,
            'taskvalue': Task.objects.last().pk,
            'offer_submit': True
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'projects/project_view.html')

    def test_project_view_POST_offer_response(self):
        self.test_project_view_POST_offer_submit()

        taskofferid = get_all_taskoffers(self.testTask)[0].id
        self.client.login(username='testUser', password='top_secret')
        response = self.client.post('/projects/1/', {
            'status': 'a',
            'feedback': 'this is a feedback',
            'taskofferid': taskofferid,
            'offer_response': True
        })
        self.assertEqual(response.status_code, 200)

    def test_project_view_POST_status_change(self):
        self.client.login(username='testUser', password='top_secret')
        response = self.client.post('/projects/1/', {
            'status': 'i',
            'status_change': True
        })
        self.assertEqual(response.status_code, 200)

    def test_project_view_POST_status_change_NoReviews(self):
        self.client.login(username='testUser_2', password='top_secret')
        response = self.client.post('/projects/' + str(Project.objects.get(title='Another test project').pk) + '/', {
            'title': 'test offer',
            'description': 'this is a description',
            'price': 200,
            'taskvalue': Task.objects.last().pk,
            'offer_submit': True
        })
        self.client.logout()

        testOffer = TaskOffer.objects.create(
            task=self.testTaskTwo,
            title='A test offer',
            description='',
            price=20,
            offerer=User.objects.get(username='testUser_2').profile,
            rating=None,
            status='a',
            feedback=''
        )

        taskofferid = get_all_taskoffers(self.testTask)[0].id
        self.client.login(username='testUser', password='top_secret')
        response = self.client.post('/projects/' + str(Project.objects.get(title='Another test project').pk) + '/', {
            'status': 'a',
            'feedback': 'this is a feedback',
            'taskofferid': taskofferid,
            'offer_response': True
        })

        response = self.client.post('/projects/' + str(Project.objects.get(title='Another test project').pk) + '/', {
            'status': 'f',
            'status_change': True
        })
        self.assertEqual(response.status_code, 200)

    def test_project_view_POST_status_change_finihed(self):
        self.client.login(username='testUser_2', password='top_secret')
        response = self.client.post('/projects/' + str(Project.objects.get(title='Another test project').pk) + '/', {
            'title': 'test offer',
            'description': 'this is a description',
            'price': 200,
            'taskvalue': Task.objects.last().pk,
            'offer_submit': True
        })
        self.client.logout()

        testRating = Rating.objects.create(
            score=2,
            written_review=''
        )

        testOffer = TaskOffer.objects.create(
            task=self.testTaskTwo,
            title='A test offer',
            description='',
            price=20,
            offerer=User.objects.get(username='testUser_2').profile,
            rating=testRating,
            status='a',
            feedback=''
        )

        taskofferid = get_all_taskoffers(self.testTask)[0].id
        self.client.login(username='testUser', password='top_secret')
        response = self.client.post('/projects/' + str(Project.objects.get(title='Another test project').pk) + '/', {
            'status': 'a',
            'feedback': 'this is a feedback',
            'taskofferid': taskofferid,
            'offer_response': True
        })

        response = self.client.post('/projects/' + str(Project.objects.get(title='Another test project').pk) + '/', {
            'status': 'f',
            'status_change': True
        })
        self.assertEqual(response.status_code, 200)

    def test_task_permissions_GET_(self):
        self.client.login(username='testUser', password='top_secret')
        response = self.client.get('/projects/1/tasks/1/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(get_user_task_permissions(self.user, self.testTask),
                         response.context['user_permissions'])

    def test_task_permissions_Hired_task(self):
        self.test_project_view_POST_offer_submit()
        self.test_project_view_POST_offer_response()
        user_pref = get_user_task_permissions(self.user_2, self.testTask)
        self.assertEqual(user_pref, {
                         'owner': False,
                         'modify': True,
                         'read': True,
                         'write': True,
                         'upload': True
                         })

    def test_task_permissions_POST_upload(self):
        self.client.login(username='testUser_2', password='top_secret')
        user_pref = get_user_task_permissions(self.user_2, self.testTask)
        self.assertEqual(user_pref, {
                         'owner': False,
                         'modify': False,
                         'read': False,
                         'write': False,
                         'view_task': False,
                         'upload': False
                         })

    # Test Offer Boundarys --------------------:

    # All valid fields:
    def test_all_valid_fields_Offer(self):
        self.client.login(username='testUser_2', password='top_secret')
        res = self.client.post('/projects/1/', self.data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(TaskOffer.objects.count(), 1)

    # Title:
    def test_empty_title(self):
        self.client.login(username='testUser_2', password='top_secret')
        data = self.data
        data['title'] = ''
        res = self.client.post('/projects/1/', self.data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(TaskOffer.objects.count(), 0)

    def test_1Char_title(self):
        self.client.login(username='testUser_2', password='top_secret')
        data = self.data
        data['title'] = 'G'
        res = self.client.post('/projects/1/', self.data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(TaskOffer.objects.count(), 1)

    def test_200Chars_title(self):
        self.client.login(username='testUser_2', password='top_secret')
        data = self.data
        data['title'] = 'vSBPavwq3oHSXNums7jeO5Wh3vnHuTZNrj7tgsrqcFfqCFUtwXzfrGMXxd9F2bfVTOik6AHXmMqE0eXnyx29E2FviewHWcPlqza3g7hxTqIG3nhwB6mRaU3kRkn7W6fc9c7HMuFSQRbURVIOTf8SPnL5jya2Wo8lJS9TWYTSLVaK9mklIUTZJm9M2momSOSxJ6XQmWUo'
        res = self.client.post('/projects/1/', self.data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(TaskOffer.objects.count(), 1)

    def test_MoreThan200Chars_title(self):
        self.client.login(username='testUser_2', password='top_secret')
        data = self.data
        data['title'] = 'vSBPavwq3oHSXNums7jeO5Wh3vnHuTZNrj7tgsrqcFfqCFUtwXzfrGMXxd9F2bfVTOik6AHXmMqE0eXnyx29E2FviewHWcPlqza3g7hxTqIG3nhwB6mRaU3kRkn7W6fc9c7HMuFSQRbURVIOTf8SPnL5jya2Wo8lJS9TWYTSLVaK9mklIUTZJm9M2momSOSxJ6XQmWUonsoit'
        res = self.client.post('/projects/1/', self.data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(TaskOffer.objects.count(), 0)

    # Description:
    def test_empty_description(self):
        self.client.login(username='testUser_2', password='top_secret')
        data = self.data
        data['description'] = ''
        res = self.client.post('/projects/1/', self.data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(TaskOffer.objects.count(), 0)

    def test_1orMoreChars_description(self):
        self.client.login(username='testUser_2', password='top_secret')
        data = self.data
        data['description'] = 'This is some text'
        res = self.client.post('/projects/1/', self.data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(TaskOffer.objects.count(), 1)

    # Price:
    def test_None_price(self):
        self.client.login(username='testUser_2', password='top_secret')
        data = self.data
        data['price'] = None
        res = self.client.post('/projects/1/', self.data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(TaskOffer.objects.count(), 0)

    def test_Zero_price(self):
        self.client.login(username='testUser_2', password='top_secret')
        data = self.data
        data['price'] = 0
        res = self.client.post('/projects/1/', self.data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(TaskOffer.objects.count(), 1)

    def test_LargeNumber_price(self):
        self.client.login(username='testUser_2', password='top_secret')
        data = self.data
        data['price'] = 5825388525200000000  # Max numbers for input to handle
        res = self.client.post('/projects/1/', self.data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(TaskOffer.objects.count(), 1)


# Output coverage tests to identify bugs related to accepting offers.
class outputOfferAccept(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()

        # Project owner
        cls.user = User.objects.create_user(
            username='testUser',
            email='test@test.com',
            password='top_secret'
        )

        # Bidder user
        cls.user_2 = User.objects.create_user(
            username='testUser_2',
            email='test@test.com',
            password='top_secret'
        )

        # Bidder 2
        cls.user_3 = User.objects.create_user(
            username='testUser_3',
            email='test@test.com',
            password='top_secret'
        )

        cls.testCategory = ProjectCategory.objects.create(name='test')

        cls.testProject = Project.objects.create(
            user=User.objects.get(username='testUser').profile,
            title="Test prosjekt",
            description='',
            category=cls.testCategory,
            status='open'
        )

    def setUp(self):

        self.testTask = Task.objects.create(
            project=self.testProject,
            title='Test task',
            description='',
            budget=20,
            status='ad',
            feedback=''
        )

        self.client.login(username='testUser_2', password='top_secret')
        response = self.client.post('/projects/1/', {
            'title': 'test offer',
            'description': 'this is a description',
            'price': 29,
            'taskvalue': Task.objects.last().pk,
            'offer_submit': True
        })

        self.client.login(username='testUser_3', password='top_secret')
        response = self.client.post('/projects/1/', {
            'title': 'test offer 2',
            'description': 'this is a description',
            'price': 30,
            'taskvalue': Task.objects.last().pk,
            'offer_submit': True
        })

        self.client.login(username='testUser', password='top_secret')

    def test_acceptOffer(self):
        taskofferid = get_all_taskoffers(self.testTask)[0].id
        res = self.client.post('/projects/' + str(Project.objects.get(title='Test prosjekt').pk) + '/', {
            'status': 'a',
            'feedback': 'this is a feedback',
            'taskofferid': taskofferid,
            'offer_response': True
        })
        self.assertTrue(
            'testUser_2' in get_project_participants(self.testProject))
        self.assertEqual(get_accepted_task_offer(self.testTask).status, 'a')
        self.assertEqual(res.context['project'], self.testProject)

    @skip('Should fail bicause not all unaccepted offers are removed from the Queryset when one is accepted')
    # As the UI explains: Accepting one offer will remove all other pending offers.
    # This seams to not be the case, since get_all_taskoffers(self.testTask) returnes a Queryset with still all offers after one is accepted.
    # In this queryset we also see that the remaining offer is still Pending, and tecnicaly should be set to Declined.
    def test_acceptOffer_notSet(self):
        taskofferid = get_all_taskoffers(self.testTask)[0].id
        res = self.client.post('/projects/' + str(Project.objects.get(title='Test prosjekt').pk) + '/', {
            'status': 'a',
            'feedback': 'this is a feedback',
            'taskofferid': taskofferid,
            'offer_response': True
        })
        self.assertTrue(
            'testUser_2' in get_project_participants(self.testProject))
        self.assertEqual(get_accepted_task_offer(self.testTask).status, 'a')
        self.assertEqual(res.context['project'], self.testProject)
        self.assertEqual(get_all_taskoffers(self.testTask)[0].status, 'a')
        self.assertEqual(get_all_taskoffers(self.testTask)
                         [1].status, 'd')  # This is p, should be d
        self.assertEqual(get_all_taskoffers(
            self.testTask).count(), 1)  # This is 2, should be 1

    def test_pendingOffer(self):
        taskofferid = get_all_taskoffers(self.testTask)[0].id
        res = self.client.post('/projects/' + str(Project.objects.get(title='Test prosjekt').pk) + '/', {
            'status': 'p',
            'feedback': 'this is a feedback',
            'taskofferid': taskofferid,
            'offer_response': True
        })
        self.assertTrue(
            'testUser_2' not in get_project_participants(self.testProject))
        self.assertEqual(get_accepted_task_offer(self.testTask), None)
        self.assertEqual(check_taskoffers(
            self.testTask, self.user_2)[0].status, 'p')
        self.assertEqual(res.context['project'], self.testProject)

    def test_declinedOffer(self):
        taskofferid = get_all_taskoffers(self.testTask)[0].id
        res = self.client.post('/projects/' + str(Project.objects.get(title='Test prosjekt').pk) + '/', {
            'status': 'd',
            'feedback': 'this is a feedback',
            'taskofferid': taskofferid,
            'offer_response': True
        })
        self.assertTrue(
            'testUser_2' not in get_project_participants(self.testProject))
        self.assertEqual(get_accepted_task_offer(self.testTask), None)
        self.assertEqual(check_taskoffers(
            self.testTask, self.user_2)[0].status, 'd')
        self.assertEqual(res.context['project'], self.testProject)

    @skip('Project status dont change from Open to In-progress')
    def test_projectStatus_OtoI(self):
        taskofferid = get_all_taskoffers(self.testTask)[0].id
        res = self.client.post('/projects/' + str(Project.objects.get(title='Test prosjekt').pk) + '/', {
            'status': 'a',
            'feedback': 'this is a feedback',
            'taskofferid': taskofferid,
            'offer_response': True
        })
        self.assertEqual(check_taskoffers(self.testTask, self.user_2)
                         [0].task.project.status, 'open')

        response = self.client.post('/projects/' + str(Project.objects.get(title='Test prosjekt').pk) + '/', {
            'status': 'i',
            'status_change': True
        })
        self.assertEqual(check_taskoffers(self.testTask, self.user_2)
                         [0].task.project.status, 'i')  # Should be i not o


class IntegrationAndSystemTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.client = Client()

        # Project owner
        cls.project_owner_user = User.objects.create_user(
            username='project_owner',
            email='test@test.com',
            password='top_secret'
        )

        # Bidder user
        cls.bidder_user1 = User.objects.create_user(
            username='bidder1',
            email='test@test.com',
            password='top_secret'
        )

        # Bidder user
        cls.bidder_user2 = User.objects.create_user(
            username='bidder2',
            email='test@test.com',
            password='top_secret'
        )

        cls.cleaningCategory = ProjectCategory.objects.create(name='cleaning')

    def setUp(self):
        self.testProject = Project.objects.create(
            user=self.project_owner_user.profile,
            title="Clean house",
            description="Clean rooms in my house",
            category=self.cleaningCategory,
            status='open'
        )

        self.testProject2 = Project.objects.create(
            user=self.project_owner_user.profile,
            title="Clean house2",
            description="Clean rooms in my house",
            category=self.cleaningCategory,
            status='open'
        )

        self.test_task_living = Task.objects.create(
            project=self.testProject,
            title="Clean living room",
            description="Use both mop and vacuum cleaner",
            budget=500,
            status='pd',  # PENDING_DELIVERY
        )

        self.test_task_kitchen = Task.objects.create(
            project=self.testProject,
            title="Clean kitchen",
            description="Use both mop and vacuum cleaner",
            budget=500,
            status='pd',  # PENDING_DELIVERY
        )

        self.test_task_living2 = Task.objects.create(
            project=self.testProject2,
            title="Clean living room2",
            description="Use both mop and vacuum cleaner",
            budget=500,
            status='pd',  # PENDING_DELIVERY
        )

        self.test_task_kitchen2 = Task.objects.create(
            project=self.testProject2,
            title="Clean kitchen2",
            description="Use both mop and vacuum cleaner",
            budget=550,
            status='pd',  # PENDING_DELIVERY
        )

        self.task_offer_data_living = {
            'title': 'Best living room cleaner',
            'description': 'Much experience',
            'price': 500,
            'taskvalue': Task.objects.get(title="Clean living room").pk,
            'offer_submit': True
        }

        self.task_offer_data_kitchen = {
            'title': 'Master kitchen cleaner',
            'description': 'And kitchen is my speciality',
            'price': 600,
            'taskvalue': Task.objects.get(title="Clean kitchen").pk,
            'offer_submit': True
        }

        self.task_offer_data_living2 = {
            'title': 'Best living room cleaner2',
            'description': 'Much experience',
            'price': 500,
            'taskvalue': Task.objects.get(title="Clean living room2").pk,
            'offer_submit': True
        }

        self.task_offer_data_kitchen2 = {
            'title': 'Master kitchen cleaner2',
            'description': 'And kitchen is my speciality',
            'price': 600,
            'taskvalue': Task.objects.get(title="Clean kitchen2").pk,
            'offer_submit': True
        }

    """Integration tests"""

    def test_submit_empty_review_form_home(self):
        self.submit_and_accept_taskoffers()

        # Submit empty form
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [],
            "written_review": []
        })
        self.client.logout()

        # Check bidder1
        self.client.login(username='bidder1', password='top_secret')
        res = self.client.get("/")
        self.assertContains(res, "You have no reviews", status_code=200)
        self.client.logout()

        # Check bidder2
        self.client.login(username='bidder2', password='top_secret')
        res = self.client.get("/")
        self.assertContains(res, "You have no reviews", status_code=200)

    def test_submit_empty_review_form_bidder(self):
        self.submit_and_accept_taskoffers()

        # Submit empty form
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [],
            "written_review": []
        })
        self.client.logout()

        # Submit new taskoffers for project2
        self.client.login(username='bidder1', password='top_secret')
        self.client.post('/projects/2/', self.task_offer_data_living2)
        self.client.logout()

        # Submit TaskOffer 2
        self.client.login(username='bidder2', password='top_secret')
        self.client.post('/projects/2/', self.task_offer_data_kitchen2)
        self.client.logout()

        # Check no reviews for bidders
        self.client.login(username="project_owner", password="top_secret")
        res = self.client.get("/projects/2/")
        self.assertContains(res, "No reviews available",
                            status_code=200, html=True, count=2)

    def test_submit_only_written_review_home(self):
        self.submit_and_accept_taskoffers()

        # Submit empty form
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [],
            "written_review": ["Very good joob!", "Excellent job!"]
        })
        self.client.logout()

        # Check bidder1
        self.client.login(username='bidder1', password='top_secret')
        res = self.client.get("/")
        self.assertContains(res, "You have no reviews", status_code=200)
        self.client.logout()

        # Check bidder2
        self.client.login(username='bidder2', password='top_secret')
        res = self.client.get("/")
        self.assertContains(res, "You have no reviews", status_code=200)

    def test_submit_only_written_review_bidder(self):
        self.submit_and_accept_taskoffers()

        # Submit empty form
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [],
            "written_review": ["Very good joob!", "Excellent job!"]
        })
        self.client.logout()

        # Submit new taskoffers for project2
        self.client.login(username='bidder1', password='top_secret')
        self.client.post('/projects/2/', self.task_offer_data_living2)
        self.client.logout()

        # Submit TaskOffer 2
        self.client.login(username='bidder2', password='top_secret')
        self.client.post('/projects/2/', self.task_offer_data_kitchen2)
        self.client.logout()

        # Check no reviews as bidder
        self.client.login(username="project_owner", password="top_secret")
        res = self.client.get("/projects/2/")
        self.assertContains(res, "No reviews available",
                            status_code=200, html=True, count=2)

    @skip('Known bug concerning setting project status to finished without accepting any taskoffers.')
    def test_project_status_to_f_no_accepted_taskoffers(self):
        self.client.login(username="project_owner", password="top_secret")
        res = self.client.post('/projects/1/', {
            'status': 'f',
            'status_change': 'True'
        })

        self.assertContains(res, '<p>Project status: Open</p>',
                            status_code=200, html=True)

    def test_only_star_home(self):
        self.submit_and_accept_taskoffers()

        # Submit star ratings
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [2, 4],
            "written_review": ["", ""]
        })
        self.client.logout()

        # Check bidder1
        self.client.login(username='bidder1', password='top_secret')
        res = self.client.post("/")
        self.assertContains(res, "score: 2", status_code=200)
        self.client.logout()

        # Check bidder2
        self.client.login(username='bidder2', password='top_secret')
        res = self.client.post("/")
        self.assertContains(res, "score: 4", status_code=200)

    def test_only_star_bidder(self):
        self.submit_and_accept_taskoffers()

        # Submit star ratings
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [2, 4],
            "written_review": ["", ""]
        })
        self.client.logout()

        # Submit new taskoffers for project2
        self.client.login(username='bidder1', password='top_secret')
        self.client.post('/projects/2/', self.task_offer_data_living2)
        self.client.logout()

        # Submit TaskOffer 2
        self.client.login(username='bidder2', password='top_secret')
        self.client.post('/projects/2/', self.task_offer_data_kitchen2)
        self.client.logout()

        # Check reviews for bidders
        self.client.login(username="project_owner", password="top_secret")
        res = self.client.get("/projects/2/")

        self.assertContains(res, "score: 2")
        self.assertContains(res, "score: 4")

    def test_full_reviews_home(self):
        self.submit_and_accept_taskoffers()

        # Submit star ratings
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [5, 4],
            "written_review": ["Perfect!", "Good job!"]
        })

        # Check bidder1
        self.client.login(username='bidder1', password='top_secret')
        res = self.client.get("/")
        self.assertContains(res, "score: 5", status_code=200)
        self.assertContains(res, "Perfect!", status_code=200)
        self.client.logout()

        # Check bidder2
        self.client.login(username='bidder2', password='top_secret')
        res = self.client.get("/")
        self.assertContains(res, "score: 4", status_code=200)
        self.assertContains(res, "Good job!", status_code=200)

    def test_full_review_bidder(self):
        self.submit_and_accept_taskoffers()

        # Submit star ratings
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [5, 4],
            "written_review": ["Perfect!", "Good job!"]
        })
        self.client.logout()

        # Submit new taskoffers for project2
        self.client.login(username='bidder1', password='top_secret')
        self.client.post('/projects/2/', self.task_offer_data_living2)
        self.client.logout()

        # Submit TaskOffer 2
        self.client.login(username='bidder2', password='top_secret')
        self.client.post('/projects/2/', self.task_offer_data_kitchen2)
        self.client.logout()

        # Check reviews for bidders
        self.client.login(username="project_owner", password="top_secret")
        res = self.client.get("/projects/2/")

        self.assertContains(res, "score: 5")
        self.assertContains(res, "Perfect!")
        self.assertContains(res, "score: 4")
        self.assertContains(res, "Good job!")

    """System tests"""

    @skip('Known bug concerning star rating values not being checked server side, only client side')
    def test_boundary_0stars(self):
        self.submit_and_accept_taskoffers()

        # Submit star ratings
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [0],
            "written_review": [""]
        })
        self.client.logout()

        # Check bidder1
        self.client.login(username='bidder1', password='top_secret')
        res = self.client.get("/")
        self.assertContains(res, "You have no reviews", status_code=200)

    def test_boundary_1stars(self):
        self.submit_and_accept_taskoffers()

        # Submit star ratings
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [1],
            "written_review": [""]
        })
        self.client.logout()

        # Check bidder1
        self.client.login(username='bidder1', password='top_secret')
        res = self.client.get("/")
        self.assertContains(res, "score: 1", status_code=200)

    def test_boundary_5stars(self):
        self.submit_and_accept_taskoffers()

        # Submit star ratings
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [5],
            "written_review": [""]
        })
        self.client.logout()

        # Check bidder1
        self.client.login(username='bidder1', password='top_secret')
        res = self.client.get("/")
        self.assertContains(res, "score: 5", status_code=200)

    @skip('Known bug concerning star rating values not being checked server side, only client side')
    def test_boundary_6stars(self):
        self.submit_and_accept_taskoffers()

        # Submit star ratings
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [6],
            "written_review": [""]
        })
        self.client.logout()

        # Check bidder1
        self.client.login(username='bidder1', password='top_secret')
        res = self.client.get("/")
        self.assertContains(res, "You have no reviews", status_code=200)

    @skip('Known bug concerning star rating values not being checked server side, only client side')
    def test_erroneous_negative(self):
        self.submit_and_accept_taskoffers()

        # Submit star ratings
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [-1],
            "written_review": [""]
        })
        self.client.logout()

        # Check bidder1
        self.client.login(username='bidder1', password='top_secret')
        res = self.client.get("/")
        self.assertContains(res, "You have no reviews", status_code=200)

    def test_erroneous_string(self):
        self.submit_and_accept_taskoffers()

        # Submit star ratings
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": ["one"],
            "written_review": [""]
        })
        self.client.logout()

        # Check bidder1
        self.client.login(username='bidder1', password='top_secret')
        res = self.client.get("/")
        self.assertContains(res, "You have no reviews", status_code=200)

    def test_erroneous_boolean(self):
        self.submit_and_accept_taskoffers()

        # Submit star ratings
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            "score": [True],
            "written_review": [""]
        })
        self.client.logout()

        # Check bidder1
        self.client.login(username='bidder1', password='top_secret')
        res = self.client.get("/")
        self.assertContains(res, "You have no reviews", status_code=200)

    # Helper method
    def submit_and_accept_taskoffers(self):
        # Submit TaskOffer 1
        self.client.login(username='bidder1', password='top_secret')
        self.client.post('/projects/1/', self.task_offer_data_living)
        self.client.logout()

        # Submit TaskOffer 2
        self.client.login(username='bidder2', password='top_secret')
        self.client.post('/projects/1/', self.task_offer_data_kitchen)
        self.client.logout()

        # Accept TaskOffer
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            'status': 'a',
            'feedback': 'perfect bidder1',
            'taskofferid': TaskOffer.objects.get(title="Best living room cleaner").id,
            'offer_response': True
        })
        self.client.post('/projects/1/', {
            'status': 'a',
            'feedback': 'perfect bidder2',
            'taskofferid': TaskOffer.objects.get(title="Master kitchen cleaner").id,
            'offer_response': True
        })
        self.client.logout()


class PreserveCorrectnessTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.client = Client()

        # Project owner
        cls.project_owner_user = User.objects.create_user(
            username='project_owner',
            email='test@test.com',
            password='top_secret'
        )
        cls.bidder_user1 = User.objects.create_user(
            username='bidder1',
            email='test@test.com',
            password='top_secret'
        )

        # Permission user
        cls.bidder_user2 = User.objects.create_user(
            username='permission_user',
            email='test@test.com',
            password='top_secret'
        )

        cls.cleaningCategory = ProjectCategory.objects.create(name='cleaning')

    def setUp(self):
        pass

    def test_project_status_open(self):
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/new/', {
            'title': 'project_title',
            'description': 'project description',
            'category_id': 1,
            'task_title': 'Clean living room',
            'task_budget': 123,
            'task_description': 'task description'
        })
        res = self.client.get('/projects/1/')

        self.assertContains(res, "Project status: Open", status_code=200)

    def test_change_permission(self):
        self.setup_projects_and_offers()

        # Change permissions of bidder
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/tasks/1/permissions/', {
            'user': 3,
            'permission': 'Write'
        })

        # Checks that permission_user now have gotten permission
        res = self.client.get('/projects/1/tasks/1/')
        self.assertContains(res, 'permission_user')

    def test_change_permission_get(self):
        self.setup_projects_and_offers()

        # Use get instead of post to try to change permission of permission_user
        self.client.login(username="project_owner", password="top_secret")
        # Get is used instead of post
        self.client.get('/projects/1/tasks/1/permissions/', {
            'user': 3,
            'permission': 'Write'
        })

        # Checks that permission_user has not gotten permission
        res = self.client.get('/projects/1/tasks/1/')
        self.assertNotContains(res, 'permission_user')

    def test_change_permission_unequal_project_id(self):
        self.setup_projects_and_offers()

        # Submit permission change for task with wrong project id
        self.client.login(username="project_owner", password="top_secret")

        self.client.post('/projects/2/tasks/1/permissions/', {
            'user': 3,
            'permission': 'Write'
        })

        # Checks that permission_user has not gotten permission
        res = self.client.get('/projects/1/tasks/1/')
        self.assertNotContains(res, 'permission_user')

    # Helper method
    def setup_projects_and_offers(self):
        # Create project 1
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/new/', {
            'title': 'project_title',
            'description': 'project description',
            'category_id': 1,
            'task_title': 'Clean living room',
            'task_budget': 123,
            'task_description': 'task description'
        })
        # Create project 2
        self.client.post('/projects/new/', {
            'title': 'project_title2',
            'description': 'project description2',
            'category_id': 1,
            'task_title': 'Clean living room2',
            'task_budget': 1232,
            'task_description': 'task description2'
        })
        self.client.logout()

        # Submit TaskOffer 1
        self.client.login(username='bidder1', password='top_secret')
        self.client.post('/projects/1/', {
            'title': 'Best living room cleaner',
            'description': 'Much experience',
            'price': 500,
            'taskvalue': Task.objects.get(title="Clean living room").pk,
            'offer_submit': True
        })
        self.client.logout()

        # Accept TaskOffer
        self.client.login(username="project_owner", password="top_secret")
        self.client.post('/projects/1/', {
            'status': 'a',
            'feedback': 'perfect bidder1',
            'taskofferid': TaskOffer.objects.get(title="Best living room cleaner").id,
            'offer_response': True
        })

        self.client.logout()


class reviewDict(TestCase):
    def setUp(self):
        self.client = Client()

        # Project owner
        self.user = User.objects.create_user(
            username='testUser',
            email='test@test.com',
            password='top_secret'
        )

        # Bidder user
        self.user_2 = User.objects.create_user(
            username='testUser_2',
            email='test@test.com',
            password='top_secret'
        )

        self.testCategory = ProjectCategory.objects.create(name='test')

        self.testProject = Project.objects.create(
            user=User.objects.get(username='testUser').profile,
            title="Test prosjekt",
            description='',
            category=self.testCategory,
            status='o'
        )

        self.testProjectTwo = Project.objects.create(
            user=User.objects.get(username='testUser').profile,
            title="Test prosjekt Two",
            description='',
            category=self.testCategory,
            status='o'
        )

        self.testTask = Task.objects.create(
            project=self.testProject,
            title='Test task',
            description='',
            budget=20,
            status='ad',
            feedback=''
        )

        self.testTaskTwo = Task.objects.create(
            project=self.testProject,
            title='Test task two',
            description='',
            budget=30,
            status='ad',
            feedback=''
        )

        self.testTaskThree = Task.objects.create(
            project=self.testProjectTwo,
            title='Test task two',
            description='',
            budget=30,
            status='ad',
            feedback=''
        )

    def test_reviewDict_with_rating(self):
        testRating = Rating.objects.create(
            score=4,
            written_review='nice job'
        )

        testOffer = TaskOffer.objects.create(
            task=self.testTaskTwo,
            title='A test offer',
            description='',
            price=20,
            offerer=User.objects.get(username='testUser_2').profile,
            rating=testRating,
            status='a',
            feedback=''
        )

        tasks = self.testProject.tasks.all()
        self.assertTrue(isinstance(
            create_review_dict(tasks).get(2)[0].review, Rating))
        self.assertEqual(create_review_dict(tasks).get(2)[0].taskoffer_id, 1)
        self.assertEqual(create_review_dict(tasks).get(2)
                         [0].task_title, self.testTaskTwo.title)
        self.assertTrue(isinstance(
            create_review_dict(tasks), (dict, list, tuple)))

    def test_reviewDict_with_twoRatings(self):

        testRating = Rating.objects.create(
            score=5,
            written_review="Perfect!"
        )

        testRating2 = Rating.objects.create(
            score=4,
            written_review="Good!"
        )

        testOffer = TaskOffer.objects.create(
            task=self.testTaskTwo,
            title='A test offer',
            description='',
            price=20,
            offerer=User.objects.get(username='testUser_2').profile,
            rating=testRating2,
            status='a',
            feedback=''
        )

        testOffer = TaskOffer.objects.create(
            task=self.testTaskTwo,
            title='A test offer',
            description='',
            price=20,
            offerer=User.objects.get(username='testUser_2').profile,
            rating=testRating,
            status='a',
            feedback=''
        )

        tasks = self.testProject.tasks.all()
        self.assertTrue(isinstance(
            create_review_dict(tasks), (dict, list, tuple)))
        self.assertTrue(len(create_review_dict(tasks).get(2)) > 1)

    def test_reviewDict_empty(self):

        testOffer = TaskOffer.objects.create(
            task=self.testTaskThree,
            title='A test offer',
            description='',
            price=20,
            offerer=User.objects.get(username='testUser_2').profile,
            rating=None,
            status='a',
            feedback=''
        )

        tasks = self.testProjectTwo.tasks.all()
        self.assertTrue(isinstance(
            create_review_dict(tasks), (dict, list, tuple)))

