from django.test import TestCase, Client
from django.contrib.auth.forms import UserCreationForm
from user.forms import SignUpForm
from django.contrib.auth.models import User
from django.urls import reverse
from projects.models import ProjectCategory
import random
import string
from unittest import skip


class SignUpFormTests(TestCase):

    def setUp(self):
        self.client = Client()

        self.testCategory = ProjectCategory.objects.create(name='test')
        self.testCategory2 = ProjectCategory.objects.create(name='test2')
        self.testCategory3 = ProjectCategory.objects.create(name='test3')

        # Pre valid data:
        self.data = {
            'username': 'testTest',
            'first_name': 'Kari',
            'last_name': 'Nordmann',
            'categories': '1',
            'company': 'NTNU',
            'email': 'kari@test.com',
            'email_confirmation': 'kari@test.com',
            'password1': 'passord123',
            'password2': 'passord123',
            'phone_number': '12345678',
            'country': 'Norge',
            'state': 'NO',
            'city': 'Trondheim',
            'postal_code': '1234',
            'street_address': 'Veien 2B'
        }

    # Helper method to generate String in needed range
    def gen_random_string(self, a, b):
        n = random.randint(a, b)
        chars = string.ascii_lowercase + string.digits
        return ''.join(random.choice(chars) for _ in range(n))

    # All valid fields:
    def test_all_valid_fields(self):
        res = self.client.post('/user/signup/', self.data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    # Username:
    def test_0char_username(self):
        data = self.data
        data['username'] = ''
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_1char_username(self):
        data = self.data
        data['username'] = self.gen_random_string(1, 1)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_150chars_username(self):
        data = self.data
        data['username'] = self.gen_random_string(150, 150)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_151chars_username(self):
        data = self.data
        data['username'] = self.gen_random_string(151, 151)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_specialCHARs_username(self):
        data = self.data
        data['username'] = self.gen_random_string(1, 150) + "@/./+/-/_"
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # First name:
    def test_0char_firstName(self):
        data = self.data
        data['first_name'] = ''
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_1char_firstName(self):
        data = self.data
        data['first_name'] = self.gen_random_string(1, 1)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_30chars_firstName(self):
        data = self.data
        data['first_name'] = self.gen_random_string(30, 30)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_30pluss_firstName(self):
        data = self.data
        data['first_name'] = self.gen_random_string(31, 31)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # Last name:
    def test_0char_flastName(self):
        data = self.data
        data['last_name'] = ''
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_1Char_lastName(self):
        data = self.data
        data['last_name'] = self.gen_random_string(1, 1)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_30Chars_lastName(self):
        data = self.data
        data['last_name'] = self.gen_random_string(30, 30)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_30pluss_lastName(self):
        data = self.data
        data['last_name'] = self.gen_random_string(31, 31)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # Categories:
    def test_None_Categories(self):
        data = self.data
        data['categories'] = None
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_digit_Categories(self):
        data = self.data
        data['categories'] = str(1)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_allDigit_Categories(self):
        data = self.data
        data['categories'] = str(ProjectCategory.objects.count())
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_digit_threeFields_Categories(self):
        res = self.client.post('/user/signup/', {
            'username': 'testTest',
            'first_name': 'Kari',
            'last_name': 'Nordmann',
            'categories': '1',
            'categories': '2',
            'categories': '3',
            'company': 'NTNU',
            'email': 'kari@test.com',
            'email_confirmation': 'kari@test.com',
            'password1': 'passord123',
            'password2': 'passord123',
            'phone_number': '12345678',
            'country': 'Norge',
            'state': 'NO',
            'city': 'Trondheim',
            'postal_code': '1234',
            'street_address': 'Veien 2B'
        })
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_moreThan_Categories(self):
        data = self.data
        data['categories'] = str(ProjectCategory.objects.count() + 1)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_Chars_Categories(self):
        data = self.data
        data['categories'] = 'G'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # Company:
    def test_zeroChars_Company(self):
        data = self.data
        data['company'] = ''
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_Chars_Company(self):
        data = self.data
        data['company'] = self.gen_random_string(30, 30)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_31Chars_Company(self):
        data = self.data
        data['company'] = self.gen_random_string(31, 31)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # Email:
    def test_0_email(self):
        data = self.data
        data['email'] = ''
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_8_email(self):
        data = self.data
        data['email'] = 'GHYIGThj'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_atSign_email(self):
        data = self.data
        data['email'] = '@'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_8to254_NoAtSign_email(self):
        data = self.data
        data['email'] = self.gen_random_string(8, 254)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_ValidEmail8_email(self):
        data = self.data
        email = self.gen_random_string(
            2, 2) + '@' + self.gen_random_string(2, 2) + '.no'
        data['email'] = email
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_255Chars_email(self):
        data = self.data
        data['email'] = self.gen_random_string(255, 255)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # Email confirmation:
    def test_notEqual_emailConfirmation(self):
        data = self.data
        # Sets email to different value than email_confirmation
        data['email'] = 'test@test.com'
        res = self.client.post('/user/signup/', data)
        test = data['email'] != data['email_confirmation']
        self.assertTrue(test)

    def test_Equal_emailConfirmation(self):
        data = self.data
        data['email'] = 'test@test.com'
        data['email_confirmation'] = 'test@test.com'
        res = self.client.post('/user/signup/', data)
        test = data['email'] == data['email_confirmation']
        self.assertTrue(test)

    # Password (This is also testing for Password confirmation when changing one of password1 or 2):
    def test_0Chars_Password1(self):
        data = self.data
        data['password1'] = ''
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_7Chars_Password1(self):
        data = self.data
        data['password1'] = self.gen_random_string(7, 7)
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_ValidPassword_Password1(self):
        data = self.data
        password = self.gen_random_string(8, 8)
        data['password1'] = password
        data['password2'] = password
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_digits_Password1(self):
        data = self.data
        data['password1'] = '123456789'
        data['password2'] = '123456789'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_tooCommon_Password1(self):
        data = self.data
        data['password1'] = 'password'
        data['password2'] = 'password'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # Phone number:
    def test_empty_phoneNumb(self):
        data = self.data
        data['phone_number'] = ''
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_OneDigit_phoneNumb(self):
        data = self.data
        data['phone_number'] = '1'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_50Digit_phoneNumb(self):
        data = self.data
        data['phone_number'] = '01234567890123456789012345678901234567890123456789'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_moreThan50Digit_phoneNumb(self):
        data = self.data
        data['phone_number'] = '0123456789012345678901234567890123456789012345678901'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # Country:
    def test_empty_country(self):
        data = self.data
        data['country'] = ''
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_1_country(self):
        data = self.data
        data['country'] = 'j'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_50_country(self):
        data = self.data
        data['country'] = 'htsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnit'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_50andMore_country(self):
        data = self.data
        data['country'] = 'htsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnitP'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # State:
    def test_empty_state(self):
        data = self.data
        data['state'] = ''
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_1_state(self):
        data = self.data
        data['state'] = 'N'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_50_state(self):
        data = self.data
        data['state'] = 'htsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnit'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_50andMore_state(self):
        data = self.data
        data['state'] = 'htsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnitP'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # City:
    def test_empty_city(self):
        data = self.data
        data['city'] = ''
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_1_city(self):
        data = self.data
        data['city'] = 'k'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_50_city(self):
        data = self.data
        data['city'] = 'htsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnit'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_50andMore_city(self):
        data = self.data
        data['city'] = 'htsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnitT'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # Postal code:
    def test_empty_postalCode(self):
        data = self.data
        data['postal_code'] = ''
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_OneDigit_postalCode(self):
        data = self.data
        data['postal_code'] = '1'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_50Digit_postalCode(self):
        data = self.data
        data['postal_code'] = '01234567890123456789012345678901234567890123456789'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_moreThan50Digit_postalCode(self):
        data = self.data
        data['postal_code'] = '0123456789012345678901234567890123456789012345678901'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # Street address:
    def test_empty_address(self):
        data = self.data
        data['street_address'] = ''
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    def test_1_address(self):
        data = self.data
        data['street_address'] = 'T'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_50_address(self):
        data = self.data
        data['street_address'] = 'htsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnit'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_50andMore_address(self):
        data = self.data
        data['street_address'] = 'htsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnithtsrvgsnitY'
        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)


# 2-way domain tests for Sign-up:
class twoWayDomain(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()

        cls.testCategory = ProjectCategory.objects.create(name='test')

    def setUp(self):
        # Pre valid data:
        self.data = {
            'username': 'Olanord',
            'first_name': 'Ola',
            'last_name': 'Nordmann',
            'categories': '1',
            'company': 'NTNU',
            'email': 'epos@psot.po',
            'email_confirmation': 'epos@psot.po',
            'password1': 'top_secret',
            'password2': 'top_secret',
            'phone_number': '12345678',
            'country': 'Norge',
            'state': 'Trøndelag',
            'city': 'Trondheim',
            'postal_code': '7011',
            'street_address': 'Droningens gate 10'
        }

    # Pass
    def test_1_signup(self):
        data = self.data
        data['street_address'] = 'Droningens gate 10'
        data['postal_code'] = '7011'
        data['city'] = 'Trondheim'
        data['state'] = 'Trøndelag'
        data['country'] = 'Norge'

        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    # Fail
    @skip('Should fail because values are not correct (Småland not in Norge)')
    def test_2_signup(self):
        data = self.data
        data['street_address'] = 'Storgatan 10'
        data['postal_code'] = '392 32'
        data['city'] = 'Kalmar'
        data['state'] = 'Småland'
        data['country'] = 'Norge'

        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    @skip('Should fail because values are not correct (Storgatan not in Trondheim)')
    def test_3_signup(self):
        data = self.data
        data['street_address'] = 'Storgatan 10'
        data['postal_code'] = '392 32'
        data['city'] = 'Trondheim'
        data['state'] = 'Trøndelag'
        data['country'] = 'Norge'

        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    @skip('Should fail because values are not correct (Trondheim not in Sverige & Småland)')
    def test_4_signup(self):
        data = self.data
        data['street_address'] = 'Droningens gate 10'
        data['postal_code'] = '392 32'
        data['city'] = 'Trondheim'
        data['state'] = 'Småland'
        data['country'] = 'Sverige'

        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    @skip('Should fail because values are not correct (7011 not in Storgatan & Trøndelag not in Sverige)')
    def test_5_signup(self):
        data = self.data
        data['street_address'] = 'Storgatan 10'
        data['postal_code'] = '7011'
        data['city'] = 'Kalmar'
        data['state'] = 'Trøndelag'
        data['country'] = 'Sverige'

        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    @skip('Should fail because values are not correct (Droningens gate not in Kalmar, Småland, Sverige)')
    def test_6_signup(self):
        data = self.data
        data['street_address'] = 'Droningens gate 10'
        data['postal_code'] = '7011'
        data['city'] = 'Kalmar'
        data['state'] = 'Småland'
        data['country'] = 'Sverige'

        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # Pass
    def test_7_signup(self):
        data = self.data
        data['password1'] = 'top_secret'
        data['password2'] = 'top_secret'

        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_8_signup(self):
        data = self.data
        data['password1'] = 'best_password'
        data['password2'] = 'best_password'

        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    # Fail
    @skip('Fail because passwords not equal')
    def test_9_signup(self):
        data = self.data
        data['password1'] = 'top_secret'
        data['password2'] = 'best_password'

        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)

    # Pass
    def test_10_signup(self):
        data = self.data
        data['email'] = 'epos@psot.po'
        data['email_confirmation'] = 'epos@psot.po'

        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    def test_11_signup(self):
        data = self.data
        data['email'] = 'test@tes.te'
        data['email_confirmation'] = 'test@tes.te'

        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(res.status_code, 302)

    # Fail
    @skip('Should fail because email dont match')
    def test_12_signup(self):
        data = self.data
        data['email'] = 'epos@psot.po'
        data['email_confirmation'] = 'test@tes.te'

        res = self.client.post('/user/signup/', data)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(res.status_code, 200)
