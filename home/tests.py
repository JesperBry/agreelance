from django.test import TestCase, Client
from .templatetags.home_extras import get_user_task_statuses, get_task_statuses
from django.contrib.auth.models import User
from projects.models import Project, Task, ProjectCategory, TaskOffer, Rating


class taskStatuses(TestCase):

    def setUp(self):
        self.client = Client()

        # Project owner
        self.user = User.objects.create_user(
            username='testUser',
            email='test@test.com',
            password='top_secret'
        )

        self.offerer = User.objects.create_user(
            username='testOfferer',
            email='test@test.com',
            password='top_secret'
        )

        self.testCategory = ProjectCategory.objects.create(name='test')

        self.testProject = Project.objects.create(
            user=User.objects.get(username='testUser').profile,
            title="Test prosjekt",
            description='',
            category=self.testCategory,
            status='o'
        )

        self.testTask = Task.objects.create(
            project=self.testProject,
            title='Test task',
            description='',
            budget=20,
            status='ad',
            feedback=''
        )

        self.testTask2 = Task.objects.create(
            project=self.testProject,
            title='Test task Two',
            description='',
            budget=40,
            status='ad',
            feedback=''
        )

        self.testTask3 = Task.objects.create(
            project=self.testProject,
            title='Test task Three',
            description='',
            budget=40,
            status='pp',
            feedback=''
        )

    def test_getTaskStatuses(self):
        data = {
            'awaiting_delivery': 2,
            'payment_sent': 0,
            'pending_payment': 1,
            'declined_delivery': 0,
            'pending_acceptance': 0
        }

        self.assertEqual(get_task_statuses(self.testProject), data)

    # Exist no offeres
    def test_getUserTaskStatuses(self):
        data = {
            'awaiting_delivery': 0,
            'payment_sent': 0,
            'pending_payment': 0,
            'declined_delivery': 0,
            'pending_acceptance': 0
        }

        self.assertEqual(get_user_task_statuses(
            self.testProject, self.user), data)

    # Exist one accepted offer
    def test_getUserTaskStatuses_withOffers(self):
        testRating = Rating.objects.create(
            score=2,
            written_review=''
        )

        testOffer = TaskOffer.objects.create(
            task=self.testTask,
            title='A test offer',
            description='',
            price=20,
            offerer=User.objects.get(username='testOfferer').profile,
            rating=testRating,
            status='a',
            feedback=''
        )

        data = {
            'awaiting_delivery': 1,
            'payment_sent': 0,
            'pending_payment': 0,
            'declined_delivery': 0,
            'pending_acceptance': 0
        }

        self.assertEqual(get_user_task_statuses(
            self.testProject, self.offerer), data)
