from django.shortcuts import render, redirect
from pprint import pprint

from projects.models import Project, TaskOffer

def home(request):
    if (request.user.is_authenticated):
        user = request.user
        user_projects = Project.objects.filter(user = user.profile)
        customer_projects = list(Project.objects.filter(participants__id=user.id).order_by().distinct())
        for team in user.profile.teams.all():
            customer_projects.append(team.task.project)
        cd = {}
        for customer_project in customer_projects:
            cd[customer_project.id] = customer_project

        customer_projects = cd.values()
        given_offers_projects = Project.objects.filter(pk__in=get_given_offer_projects(user)).distinct()
        received_reviews = []
        taskoffers = TaskOffer.objects.filter(offerer=user.profile)
        for taskoffer in taskoffers:
            if taskoffer.status == 'a' and taskoffer.rating != None:
                received_reviews.append((taskoffer.id, taskoffer.task.title, taskoffer.rating))
        return render(
        request,
        'index.html',
        {
            'user_projects': user_projects,
            'customer_projects': customer_projects,
            'given_offers_projects': given_offers_projects,
            'received_reviews': received_reviews
        })
    else:
        return redirect('projects')

# taskoffers = task.taskoffer_set.filter(offerer=user.profile)

def get_given_offer_projects(user):
    project_ids = set()

    for taskoffer in user.profile.taskoffer_set.all():
        project_ids.add(taskoffer.task.project.id)

    return project_ids
